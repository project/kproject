<?php

function kpledger_drush_command() {
  return array('kpledger sync' => array('description' => 'sync ledger customers with kproject kclients', 'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL, 'callback' => 'kpledger_sync'));
}

function kpledger_sync() {
  $user = drush_get_option('user');
  $pass = drush_get_option('pass');
  $host = drush_get_option('host');
  $db   = drush_get_option('db');
  $ledgerclients = array();
  drush_log("connecting to pgsql server");
  if ($dbconn = pg_connect("host=$host dbname=$db user=$user password=$pass")) {
    if ($result = pg_query("SELECT id AS ledgerid, name, address1 || '\n' || address2 || '\n' || city || '\n' || state || '\n' || zipcode || '\n' || country || '\n' || contact || '\n' || phone || '\n' || fax || '\n' || email || '\n' || notes AS body FROM customer;")) {
      while ($row = pg_fetch_assoc($result)) {
        $ledgerclients[] = $row;
      }
    } else {
      drush_log("query failed: " . pg_last_error());
    }
  } else {
    drush_log("can't connect to database server: " . pg_last_error());
  }
  pg_close();
  
  foreach ($ledgerclients as $ledgerclient) {
    kpledger_sync_client($ledgerclient);
  }
}
