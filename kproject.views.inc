<?php
/**
 * Implementation of hook_views_data().
 */
function kproject_views_data() {
  // // kclient_node table
  // 
  $data['ktask_kclient_node']['table'] = array(
    'group' => 'KProject',
  );
  
  $data['ktask_kclient_node']['table']['join']['node'] = array(
    'table' => 'node',
    'left_table' => 'ktask_kcontract',
    'left_field' => 'parent',
    'field' => 'nid'
  );
  
  $data['ktask_kclient_node']['title'] = array(
    'title' => t('client title'),
    'help' => t('The title for the client parent of the task.'), // The help that appears on the UI,
    'field' => array(
    'handler' => 'views_handler_field_node',
    'click sortable' => TRUE,
    )
  );
  
  // Workflow (tasks and contracts)
  
  $workflow = array(
    'estimate' => array(
      'title' => t('Estimate'),
      'help' => t('Estimate in hours'),
      'field' => array(
        'handler' => 'views_handler_field_xss'
      ),
    )
  );
  
  // kcontract table
  
  $data['ktask_kcontract']['table'] = array(
    'group' => 'KProject',
  );
  
  $data['ktask_kcontract']['table']['join']['node'] = array(
    'table' => 'kcontract',
    'left_table' => 'ktask',
    'left_field' => 'parent',
    'field' => 'nid'
  );
  
  $data['ktask_kcontract']['parent'] = array(
        'title' => t('Parent client id'),
        'help' => t('The node ID of the parent of this node.'), // The help that appears on the UI,
        'field' => array(
          'handler' => 'views_handler_field_numeric',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_numeric',
          'name field' => 'title', // the field to display in the summary.
          'numeric' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_kproject_parent',
        ),
      );

  // kcontract_node table

  $data['ktask_kcontract_node']['table'] = array(
    'group' => 'KProject',
  );
  
  $data['ktask_kcontract_node']['table']['join']['node'] = array(
    'table' => 'node',
    'left_table' => 'ktask',
    'left_field' => 'parent',
    'field' => 'nid'
  );
  
  $data['ktask_kcontract_node']['title'] = array(
    'title' => t('contract title'),
    'help' => t('The title for the contract parent of the task.'), // The help that appears on the UI,
    'field' => array(
    'click sortable' => TRUE,
    )
  );
  
  

  // ktask table

  $data['ktask']['table'] = array(
    'group' => 'KProject'
  );
  
  $data['ktask']['table']['join']['node'] = array(
    'left_field' => 'nid',
    'field' => 'nid'
  );
  
  // ktask fields
  
  $data['ktask']['parent'] = array(
    'title' => t('parent'),
    'help' => t('The node parent of the task.'), // The help that appears on the UI,
    // Information for displaying the nid
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'title', // the field to display in the summary.
      'numeric' => TRUE,
    ),
    'filter' => array(
		      'handler' => 'views_handler_filter_kproject_parent',
        ),

  );
  
    $data['ktask_kcontract']['alias'] = array(
    'title' => t('contract alias'),
    'help' => t('The alias for the contract parent of the task.'), // The help that appears on the UI,
    'field' => array(
      // 'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
		     ),
  );

  $data['ktask_kcontract']['estimate'] = array(
    'title' => t('contract estimate'),
    'help' => t('Contract estimate.'), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    )
  );

  $data['ktask_kcontract']['begin'] = array(
    'title' => t('Begin'),
    'help' => t('When the work should start for this contract.'), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    )
  );

  $data['ktask_kcontract']['end'] = array(
    'title' => t('End'),
    'help' => t('When the work should end for this contract.'), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    )
  );
  
  $data['ktask_kcontract']['lead_username'] = array(
    'title' => t('Lead user name'),
    'help' => t('Lead user name for this contract.'), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field_kproject_username',
      'click sortable' => TRUE,
    ),
  );
  
  

  // Preserving compatibility with old fields
  
  $node_types = node_get_types();
  
  
  $data['kpunch']['bill_reference'] = array(
    'field' => array(
      'handler' => 'views_handler_field',
      'title' => t('Bill reference'),
      'help' => t('Bill reference for this punch'),
    )
  );
  

  return $data;
}

function kproject_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'kproject').'/views',
    ),
    'handlers' => array(
      'views_handler_field_kproject_hierarchy' => array(
        'parent' => 'views_handler_field',
      ),
      'views_handler_filter_kproject_parent' => array(
        'parent' => 'views_handler_filter_equality',
      ),
      'views_handler_field_kproject_username' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}

function kproject_views_default_views() {

  // TODO: need a little cleanup

  $types = node_get_types();

  foreach (kproject_tree() as $type => $branch) {
    $branch_type = $types[$type];
    $label = $branch_type->title_label;
    $parent = $branch['parent'];
    $parent_label = $types[$parent]->title_label;

    $view = new view;
    $view->name = "{$type}_for_{$parent}";
    $view->description = '';
    $view->tag = 'kproject';
    $view->view_php = '';
    $view->base_table = 'node';
    $view->is_cacheable = FALSE;
    $view->api_version = 2;
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
    $handler = $view->new_display('default', 'Default', 'default');
    $handler->override_option('fields', array(
      'title' => array(
        'label' => $label,
        'alter' => array(
          'alter_text' => 0,
          'text' => '',
          'make_link' => 0,
          'path' => '',
          'alt' => '',
          'prefix' => '',
          'suffix' => '',
          'help' => '',
          'trim' => 0,
          'max_length' => '',
          'word_boundary' => 1,
          'ellipsis' => 1,
          'strip_tags' => 0,
          'html' => 0,
        ),
        'link_to_node' => 1,
        'exclude' => 0,
        'id' => 'title',
        'table' => 'node',
        'field' => 'title',
        'relationship' => 'none',
      ),
      'edit_node' => array(
        'label' => 'Operations',
        'alter' => array(
          'alter_text' => 0,
          'text' => '',
          'make_link' => 0,
          'path' => '',
          'alt' => '',
          'prefix' => '',
          'suffix' => '',
          'help' => '',
          'trim' => 0,
          'max_length' => '',
          'word_boundary' => 1,
          'ellipsis' => 1,
          'strip_tags' => 0,
          'html' => 0,
        ),
        'text' => 'modif',
        'exclude' => 0,
        'id' => 'edit_node',
        'table' => 'node',
        'field' => 'edit_node',
        'relationship' => 'none',
      ),
      'delete_node' => array(
        'label' => 'Delete link',
        'alter' => array(
          'alter_text' => 0,
          'text' => '',
          'make_link' => 0,
          'path' => '',
          'alt' => '',
          'prefix' => '',
          'suffix' => '',
          'help' => '',
          'trim' => 0,
          'max_length' => '4',
          'word_boundary' => 0,
          'ellipsis' => 0,
          'strip_tags' => 0,
          'html' => 0,
        ),
        'text' => 'supr',
        'exclude' => 0,
        'id' => 'delete_node',
        'table' => 'node',
        'field' => 'delete_node',
        'relationship' => 'none',
      ),
    ));
    $handler->override_option('sorts', array(
      'title' => array(
        'order' => 'ASC',
        'id' => 'title',
        'table' => 'node',
        'field' => 'title',
        'relationship' => 'none',
      ),
    ));
    $handler->override_option('arguments', array(
      'parent' => array(
        'default_action' => 'empty',
        'style_plugin' => 'default_summary',
        'style_options' => array(),
        'wildcard' => 'all',
        'wildcard_substitution' => 'All',
        'title' => '',
        'default_argument_type' => 'fixed',
        'default_argument' => '',
        'validate_type' => 'none',
        'validate_fail' => 'not found',
        'break_phrase' => 0,
        'not' => 0,
        'id' => 'parent',
        'table' => $type,
        'field' => 'parent',
        'relationship' => 'none',
        'default_options_div_prefix' => '',
        'default_argument_user' => 0,
        'default_argument_fixed' => '',
        'default_argument_php' => '',
        'validate_argument_node_access' => 0,
        'validate_argument_nid_type' => 'nid',
        'validate_argument_type' => 'tid',
        'validate_argument_is_member' => 0,
        'validate_argument_php' => '',
        'validate_user_argument_type' => 'uid',  
        'validate_argument_transform' => 0,
        'validate_user_restrict_roles' => 0,
      ),
    ));
    $handler->override_option('access', array(
      'type' => 'none',
    ));
    $handler->override_option('title', $label . ' for this ' . $parent_label);
    $handler->override_option('empty', 'No ' . $label . ' for this ' . $parent_label . '.');
    $handler->override_option('empty_format', '3');
    $handler->override_option('items_per_page', 20);
    $handler->override_option('use_pager', 'mini');
    $handler->override_option('style_plugin', 'table');
    $handler->override_option('style_options', array(
      'grouping' => '',
      'override' => 1,
      'sticky' => 0,
      'order' => 'asc',
      'columns' => array(
        'title' => 'title',
        'edit_node' => 'edit_node',
        'delete_node' => 'edit_node',
      ),
      'info' => array(
        'title' => array(
          'sortable' => 0,
          'separator' => '',
        ),
        'edit_node' => array(
          'separator' => ' ',
        ),
        'delete_node' => array(
          'separator' => '',
        ),
      ),
      'default' => '-1',
    ));

    $views[$view->name] = $view;

  }

  return $views;
}
