<?php
/**
 * Field handler to provide simple renderer that allows using a themed user link
 */
class views_handler_field_kproject_parent extends views_handler_field {
  /**
   * Add uid in the query so we can test for anonymous if needed.
   */
  // function init(&$view, &$data) {
  //   parent::init($view, $data);
  //     $this->additional_fields['begin'] = array('field' => 'begin');
  //     $this->additional_fields['end'] = array('field' => 'end');
  // }  
  
  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }
  
  function render($values) {
     return print_r($this,TRUE);
  }    
}

