<?php

/**
 * Field handler to provide display of kproject hierarchy
 */
class views_handler_field_kproject_hierarchy extends views_handler_field {  

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['separator'] = array(
      '#type' => 'textfield',
      '#title' => t('Separator'),
      '#description' => t('Separator between each item of the hierarchy.'),
      '#default_value' => $this->options['separator'] ? $this->options['separator'] : '/',
    );

    $form['tasktoclient'] = array(
      '#type' => 'checkbox',
      '#title' => t('Task to client'),
      '#description' => t('If checked, the hierarchy will be from task to client otherwise from client to task.'),
      '#default_value' => !empty($this->options['tasktoclient']),
    );

    $form['tree'] = array(
      '#type' => 'fieldset',
      '#title' => t('Elements to display'),
    );

    foreach (kproject_tree(TRUE) as $elem => $dummy) {
      $form['tree'][$elem] = array(
        '#type' => 'checkbox',
        '#title' => $elem,
        '#default_value' => isset($this->options['tree'][$elem]) ? !empty($this->options['tree'][$elem]) : TRUE,
      );
    } 

  }

  function render($values) {
    $hierarchy = kproject_get_node_hierarchy($values->ktask_kpunch_nid);

    $tree = array_keys(kproject_tree(TRUE));
    foreach ($tree as $name) {
      if ($this->options['tree'][$name]) {
        $elems[] = $hierarchy[$name. '_title'];
      }
    }

    if ($this->options['tasktoclient']) {
      $elems = array_reverse($elems);
    }
    return implode($this->options['separator'], $elems);
  }    
}


